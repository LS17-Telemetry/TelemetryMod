# TelemetryMod

This is the Mod for LS17, which collects Data, displays them ingame and writes it to a XML.



## Installation

OS X & Linux:

```

```

Windows:

```

```

## Usage example


## Development setup


## Release History


## Meta


## Contributing


<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/yourname/yourproject/wiki